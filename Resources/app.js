/*
 * Single Window Application Template:
 * A basic starting point for your application.  Mostly a blank canvas.
 * 
 * In app.js, we generally take care of a few things:
 * - Bootstrap the application with any data we need
 * - Check for dependencies like device type, platform version or network connection
 * - Require and open our top-level UI component
 *  
 */

// 判斷 Titanium 版本小於1.8
if (Ti.version < 1.8 ) {
	alert('Sorry - this application template requires Titanium Mobile SDK 1.8 or later');	  	
}

// 自己呼叫自己本身的執行方法
(function() {
	// 產生需要的變數
	var osname = Ti.Platform.osname,	// 裝置系統環境名稱
		version = Ti.Platform.version,	// 裝置系統環境版本
		height = Ti.Platform.displayCaps.platformHeight,	// 裝置顯示的高度
		width = Ti.Platform.displayCaps.platformWidth;		// 裝置顯示的寬度
	
	// 檢查是否為平板
	var isTablet = osname === 'ipad' || (osname === 'android' && (width > 899 || height > 899));
	
	var Window;
	if (isTablet) {
		// 平板載入檔案
		Window = require('ui/tablet/ApplicationWindow');
	}
	else {
		if (osname === 'android') {
			// Android 載入檔案
			Window = require('ui/handheld/android/ApplicationWindow');
		}
		else {
			// iPhone 載入檔案
			Window = require('ui/handheld/ApplicationWindow');
		}
	}
	
	// 將回傳的底層 Window 視窗開啟
	new Window().open();
})();
