//Application Window Component Constructor
function ApplicationWindow() {
	// 載入組件的需要檔案
	var FirstView = require('ui/common/FirstView');
		
	// 建立底層 Window 視窗
	var self = Ti.UI.createWindow({
		backgroundColor:'#ffffff'
	});
	
	// 建立顯示的 Window 視窗
	var win = Ti.UI.createWindow({
		title: 'BarryBlog',
		backgroundColor:'#FFFFFF'
	});
	
	// 建立頂部元件
	var nav = Ti.UI.iPhone.createNavigationGroup({
		// 此元件屬於的 Window 視窗
		window: win
	});
	// 將頂部元件放入底層 Window 視窗
	self.add(nav);
		
	// 建構UI元件，並帶入參數
	var firstView = new FirstView({
		nav: nav
	});
	
	// 將UI元件放入顯示的 Window 視窗
	win.add(firstView);
	
	// 回傳底層 Window 視窗
	return self;
}

//make constructor function the public component interface
module.exports = ApplicationWindow;
