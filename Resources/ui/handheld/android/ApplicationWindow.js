//Application Window Component Constructor
function ApplicationWindow() {
	// 載入組件的需要檔案
	var FirstView = require('ui/common/FirstView');
		
	// 建立底層 Window 視窗
	var self = Ti.UI.createWindow({
		title: 'BarryBlog',
		backgroundColor:'#ffffff',
		navBarHidden:false,
		exitOnClose:true
	});
		
	// 建構UI元件，並帶入參數
	var firstView = new FirstView();
	
	// 將UI元件放入顯示的 Window 視窗
	self.add(firstView);
	
	// 回傳底層 Window 視窗
	return self;
}

//make constructor function the public component interface
module.exports = ApplicationWindow;
