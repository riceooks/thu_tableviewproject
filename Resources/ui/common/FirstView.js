//FirstView Component Constructor
function FirstView(object) {
	// 建立此區域畫面
	var self = Ti.UI.createView();

	// 建立 TableView
	var tableView = Ti.UI.createTableView();
	self.add(tableView);

	// 建立 ActivityIndicator 載入效果
	var indStyle;
	if (Ti.Platform.osname === 'iphone') {
		// 當iPhone平台時，必須使用以下效果
		indStyle = Ti.UI.iPhone.ActivityIndicatorStyle.DARK;
	} else {
		// 當Android平台時，必須使用以下效果
		indStyle = Ti.UI.ActivityIndicatorStyle.DARK;
	}
	var indicator = Ti.UI.createActivityIndicator({
		// 訊息文字
		message : '載入中...',
		// 文字顏色
		color : '#000000',
		// 載入樣式
		style : indStyle,
		// 寬度
		width : Ti.UI.SIZE
	});
	// 將 ActivityIndicator 放入區域畫面
	self.add(indicator);
	// 顯示 ActivityIndicator 載入效果
	indicator.show();

	// 建立 HTTPClient 用戶端呼叫方法
	var jhr = Ti.Network.createHTTPClient({
		// HTTPClient 成功時載入
		onload : function() {

			// 回傳JSON資料並解析
			var json = JSON.parse(jhr.responseText);
			
			// 產生一個陣列，稍後存放要顯示在TableView的資料
			var rows = [];
			
			/** 使用for迴圈解析JSON資料
			 * for(索引 in 陣列)
			 */
			for (row in json.posts) {
				// 每次解析一筆資料放入rows
				rows.push({
					id : json.posts[row].ID,
					title : json.posts[row].post_title,
					content : json.posts[row].post_content,
					color : '#000000'
				});
			}
			// 將解析完畢的資料陣列給予TableView
			tableView.data = rows;

			// TableView 點擊觸發事件
			tableView.addEventListener('click', function(event) {
				// 建立 Window 視窗
				var win = Ti.UI.createWindow({
					// 標題
					title : event.rowData.title,
					// 背景顏色
					backgroundColor : '#ffffff'
				});

				// 網頁瀏覽視窗元件
				var web = Ti.UI.createWebView({
					// HTML代碼
					html : event.rowData.content
				});
				// 將網頁瀏覽元件放入Window視窗
				win.add(web);

				if (Ti.Platform.osname === 'iphone') {
					// 當平台為iPhone時，必須使用Nav元件開啟視窗，才為自動產生頂部返回功能
					object.nav.open(win);
				} else {
					// 當平台為Android時，需要告訴Window視窗元件，載入時不要隱藏Title bar及按下返回鍵不要將應用程式最上層視窗關閉
					win.navBarHidden = false;
					win.exitOnClose = false;
					
					// 開啟Window視窗
					win.open();
				}
			});

			// 隱藏 ActivityIndicator 載入效果
			indicator.hide();
		},

		// HTTPClient 發生錯誤時載入
		onerror : function(e) {
			// 彈出訊息視窗，訊息為帶入的文字
			alert(e.error);
			
			// 隱藏 ActivityIndicator 載入效果
			indicator.hide();
		},

		// HTTPClient 載入最大限時
		timeout : 8000
	});

	/** HTTPClient open方法
	 * @param String method HTTP協定的方法(GET/POST)
	 * @param String url HTTPClient呼叫的網址
	 */ 
	jhr.open('GET', 'http://barryblog.appsgoo.com/barry-json.php?action=posts');

	// HTTPClient 對伺服器發送請求
	jhr.send();

	// 回傳區域畫面
	return self;
}

module.exports = FirstView;
